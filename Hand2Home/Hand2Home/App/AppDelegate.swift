//
//  AppDelegate.swift
//  Hand2Home
//
//  Created by YASH on 1/26/18.
//  Copyright © 2018 YASH. All rights reserved.
//


//TODO: - Account Details
/*
 
 itunes Account : UserName/Password : developer.dignizant@gmail.com /  Digni@tech_4774

 Firebase Account : UserName/Password : aliamar7033@gmail.com/aliamar@123
 
 */

import UIKit
import Firebase
import UserNotifications
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import CoreLocation
import GoogleMaps
import GooglePlaces


var isNotificationScreen = Bool()

//
//protocol appDelegateProtocol {
//
//    func didUpdateLocation(lat:Double,lon:Double)
//
//}
//

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {
    
    
    var locationManager = CLLocationManager()
    var window: UIWindow?
    
//    var delgate : appDelegateProtocol?
    
//    var locationManager: Location!
    
    var navigation = UINavigationController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //set Language
        
        //TODO: - Set Language Swedish / English
        
        setLangauge(language: Swedish)
        
//        setUpQuickLocationUpdate()
//
        GMSServices.provideAPIKey("AIzaSyDmEatsuYs_qssR7U3zvavPIGKbdZp4VF4")
        GMSPlacesClient.provideAPIKey("AIzaSyDmEatsuYs_qssR7U3zvavPIGKbdZp4VF4")
        
        FirebaseApp.configure()
        
        application.applicationIconBadgeNumber = 0
        
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                
                if granted {
                    
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                    
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "grantedFalse"), object: nil)
                }
                
            }
        }
        else
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        let launchBefore = UserDefaults.standard.bool(forKey: "launchBefore")
        print("LaunchBefore : \(launchBefore)")
        
        if !launchBefore
        {
            
            let mapvc = mainStoryboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            mapvc.isFirstTimeLocationSet = "0"
            navigation.pushViewController(mapvc, animated: false)
            
        }
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
      
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if isNotificationScreen == true
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificaionUpdated"), object: nil)
            
        }
        else
        {
             completionHandler([.alert, .badge, .sound])
        }
        
        print("User Info = ",notification.request.content.userInfo)
        
       
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
       // self.listAPI()
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error)")
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tap on Banner Push for ios 10 version")
        
        let dict = response.notification.request.content.userInfo
        print("dict: \(dict)")
        print("JSON DICt - ",JSON(dict))
        let jsonDict = JSON(dict)
        
        if jsonDict["type"].stringValue == "craftman_notify"
        {
            
            let craft_id = jsonDict["craftman_id"].stringValue
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToDetails"), object: craft_id)
            
        }
        
        completionHandler()
        
        }
    
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        Messaging.messaging().connect() { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    //MARK: - Convert to dictionary
    
    func convertToDictionary(text: String) -> NSMutableDictionary
    {
        var dict = NSMutableDictionary()
        if let data = text.data(using: .utf8)
        {
            do
            {
                dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
                return dict
            } catch
            {
                print(error.localizedDescription)
            }
        }
        return dict
    }

    //TODO: - Version 1
    //MARK: - List API of category
    /*
    func listAPI()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(userURL)\(categoryList)"
            
            print("URL: \(url)")
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : "1"]
            
            print("param :\(param)")
            
            
//            if isShowLoader == true
//            {
//                self.lineBar.isHidden = false
//                linearBar?.startAnimation()
//            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                if isShowLoader == true
//                {
//                    linearBar?.stopAnimation()
//                }
                
//                self.lineBar.isHidden = true
//                self.refreshControl.endRefreshing()
//
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadColection"), object: json["data"].arrayValue)
                        
//                        self.arrayCategory = []
//
//                        var aryData = json["data"].arrayValue
//                        aryData = self.arrayCategory + aryData
//                        self.arrayCategory = aryData
//                        self.collectionVw.reloadData()
                        
                    }
                    else
                    {
                        makeToast(strMessage: "\(json["msg"].stringValue)")
                    }
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    */
    
    //TODO: - Version 2
    
/*    func listAPI()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(userURL)\(categoryListV2)"
            
            print("URL: \(url)")
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : "1"]
            
            print("param :\(param)")
            
            
            //            if isShowLoader == true
            //            {
            //                self.lineBar.isHidden = false
            //                linearBar?.startAnimation()
            //            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //                if isShowLoader == true
                //                {
                //                    linearBar?.stopAnimation()
                //                }
                
                //                self.lineBar.isHidden = true
                //                self.refreshControl.endRefreshing()
                //
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadColection"), object: json["data"].arrayValue)
                        
                        //                        self.arrayCategory = []
                        //
                        //                        var aryData = json["data"].arrayValue
                        //                        aryData = self.arrayCategory + aryData
                        //                        self.arrayCategory = aryData
                        //                        self.collectionVw.reloadData()
                        
                    }
                    else
                    {
                        makeToast(strMessage: "\(json["msg"].stringValue)")
                    }
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    
   */
    
    
}


/*
extension AppDelegate:CLLocationManagerDelegate
{
    func setUpQuickLocationUpdate()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.activityType = .fitness
        // locationManager.pausesLocationUpdatesAutomatically = true
        // locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let latestLocation = locations.first
        {
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            //  KSToastView.ks_showToast("Location Update Successfully", duration: ToastDuration)
            if userLocation?.coordinate.latitude != latestLocation.coordinate.latitude && userLocation?.coordinate.longitude != latestLocation.coordinate.longitude
            {
                userLocation = latestLocation
                /*lattitude = latestLocation.coordinate.latitude
                 longitude = latestLocation.coordinate.longitude*/
                print("lattitude:- \(String(describing: userLocation?.coordinate.latitude) ?? "0.0"), longitude:- \(String(describing: userLocation?.coordinate.longitude) ?? "0.0")")
               
            //delgate?.didUpdateLocation(lat:latestLocation.coordinate.latitude,lon:latestLocation.coordinate.longitude)
                
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
              //  openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
              //  openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error :- \(error)")
    }
    
    //MARK:- open Setting
    
    func openSetting()
    {
        
        let alertController = UIAlertController (title: mapping.string(forKey: "Hand2Home_key"), message: mapping.string(forKey: "Please_allow_location_key"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: mapping.string(forKey: "Setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        //     let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
        //    alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
*/

