//
//  BasicStuff.swift
//  Hand2Home
//
//  Created by YASH on 1/26/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit
import Toaster
import LinearProgressBarMaterial
import CoreLocation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import MBProgressHUD

//MARK: - default setup

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

let mapping:StringMapping = StringMapping.shared()
let Defaults = UserDefaults.standard
var isEnglish:Bool = true

let appdelgate = UIApplication.shared.delegate as! AppDelegate

// MARK: -  For Loader LinearProgressBar Process

var linearBar: LinearProgressBar?

//MARK: - Location Manager

var userLocation : CLLocation?


func setLangauge(language:String)
{
    
    if language == Swedish
    {
        isEnglish = false
        Defaults.set(Swedish, forKey: "language")
        Defaults.set(1, forKey: "lang")
        Defaults.synchronize()
    }
    else
    {
        isEnglish = true
        Defaults.set(English, forKey: "language")
        Defaults.set(0, forKey: "lang")
        Defaults.synchronize()
        
    }
    print("Language - ",Defaults.value(forKey: "language"));
    
    StringMapping.shared().setLanguage()
}


//MARK: - Toaster Method

func makeToast(strMessage : String){
    
    Toast(text: strMessage, delay: 0.0, duration: Delay.long).show()
    
}


//MARK: - Common method for get details

func getSetLocation(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "SetLocation") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}



extension UIViewController
{
    
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    
    func configureLinearProgressBar()
    {
        linearBar?.backgroundColor = UIColor.white
        linearBar?.progressBarColor = MySingleton.sharedManager.themeGreenColor
        linearBar?.heightForLinearBar = 5
    }
    
    func setupNavigationBar(titleText:String)
    {
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeGreenColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

    }
    
    func setupNavigationbarwithBackButton(titleText:String)
    {
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_header_white"), style: .plain, target: self, action: #selector(backAction))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        self.navigationItem.titleView = HeaderView

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeGreenColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func backAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Valid Email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }

    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = MySingleton.sharedManager.themeGreenColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    //MARK: - JSON to string
    
    func JSONtoString(JSON: JSON) -> String
    {
        let paramsString = JSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        return paramsString
    }
    
    //MARK: - MBProgresHUD method
    
    func showAdded(to view: UIView, animated: Bool) {
        
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: view, animated: true)
        }
        
    }
    
    func hide(for view: UIView, animated: Bool){
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true)
            
        }

    }
    
}
