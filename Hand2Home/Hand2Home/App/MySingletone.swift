//
//  MySingletone.swift
//  Hand2Home
//
//  Created by YASH on 1/26/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


class MySingleton
{
    // Declare class instance property
    static let sharedManager = MySingleton()
    
    //MARK: - Application color theme declaration
    
    var themeGreenColor = UIColor()
    
    var themeLightGrayColor = UIColor()
    
    var themeDarkGrayColor = UIColor()

    //MARK: - UIScreen Bounds
    var screenRect = CGRect()
    
    
    //MARK: - Initializer of Class
    // @desc Declare an initializer
    // Because this class is singleton only one instance of this class can be created
    /// Initialization of variables with default value
    init()
    {
        //Shared object created of Datamanager
        
        themeGreenColor = UIColor.init(red: 63/255, green: 182/255, blue: 86/255, alpha: 1.0)
        
        themeLightGrayColor = UIColor.init(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        
        themeDarkGrayColor = UIColor.init(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
        
        screenRect = UIScreen.main.bounds
        
    }
    
    /* Helvetica.ttf*/
 
    //MARK: - Get Custom Fonts With size
    /// Get custom regular type font with size
    ///
    /// - Parameter fontSize: font size in Int dataType
    /// - Returns: UIFont with size
    
    func getFontForTypeNormalWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Helvetica", size: CGFloat(fontSize))!
    }
    
    
    
}
