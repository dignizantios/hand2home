//
//  HomeCollectionCell.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblName.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 16)
        
        // Initialization code
    }

}
