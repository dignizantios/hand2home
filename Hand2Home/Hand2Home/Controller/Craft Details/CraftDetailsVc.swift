//
//  CraftDetailsVc.swift
//  Hand2Home
//
//  Created by YASH on 29/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import LinearProgressBarMaterial
import DropDown
import MessageUI

class CraftDetailsVc: UIViewController
{
    //MARK: - Outlet
    
    @IBOutlet weak var lineBar: UIView!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var vwWebSite: UIView!
    
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblWebsite: UILabel!
    
    @IBOutlet weak var lblWebsiteValue: UILabel!
    
    @IBOutlet weak var lblNameValue: UILabel!
    
    @IBOutlet weak var lblPhoneValue: UILabel!
    
    @IBOutlet weak var lblLocationValue: UILabel!
    
    @IBOutlet weak var lblEmailValue: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lbljobName: UILabel!
    
    @IBOutlet weak var txtFullName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtJob: UITextField!
    
    @IBOutlet weak var txtvwDescription: UITextView!
    
    @IBOutlet weak var lblPlaceholderDescription: UILabel!
    
    @IBOutlet weak var btnSend: CustomButton!
    
    @IBOutlet weak var vwForm: UIView!
    
    @IBOutlet weak var vwFullName: UIView!
    
    @IBOutlet weak var vwJob: UIView!
    
    @IBOutlet weak var vwEmail: UIView!
    
    @IBOutlet weak var vwPhone: UIView!
    
    @IBOutlet weak var vwBtn: UIView!
    @IBOutlet weak var vwDescription: UIView!
    
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnEmail: UIButton!
    
    
    //@IBOutlet weak var heightConstantvwForm: NSLayoutConstraint!
    //MARK: - Variable
    
    var strCategoryID = ""
    var strJobID = ""
    var strJobName = ""
    
    var strCraftManID = ""
    
    var jobDD = DropDown()
//    var arrayJob = NSMutableArray()
    var aryJSONJob : [JSON] = []
    
    var dictDetails = JSON()
    
    var strPhoneNumber = ""
    var strEmail = ""
    
    
    var isFromNotificationList = ""
    /*
     
     isFromNotificationList == "0" (From notification list / push)
     isFromNotificationList == "1" (From Other Screen)
     
     */
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("strCraftManID : \(strCraftManID)")
        
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        self.view.addSubview(linearBar!)
        lineBar.isHidden = true
        
        configureLinearProgressBar()
        
        print("DictDetails : \(dictDetails)")
        
        if isFromNotificationList == "0"
        {
            self.craftMainDetailsAPI()
        }
        else
        {
            self.Details(dictDetails: dictDetails)
        }
        
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "Details_key"))
        setupUI()
    
    }

    
    func setupUI()
    {
        [txtFullName,txtEmail,txtPhone,txtJob].forEach { (txtfld) in
            
            txtfld?.delegate = self
            txtfld?.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 16)
        }
        
        [lblNameValue,lblPhoneValue,lblLocationValue,lblEmailValue,lblWebsiteValue].forEach { (lbl) in
            lbl?.textColor = MySingleton.sharedManager.themeGreenColor
        }
        
        addDoneButtonOnKeyboard(textfield: txtPhone)
        
        lblName.text = mapping.string(forKey: "Name_key")
        lblPhone.text = mapping.string(forKey: "Phone_key")
        lblLocation.text = mapping.string(forKey: "Location_key")
        lblEmail.text = mapping.string(forKey: "Email_key")
        lblWebsite.text = mapping.string(forKey: "Website_key")
       
        lblDescription.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        lblDescription.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        
        lblPlaceholderDescription.text = mapping.string(forKey: "DESCRIPTION_key")
        txtFullName.placeholder = mapping.string(forKey: "FULL_NAME_key")
        txtEmail.placeholder = mapping.string(forKey: "EMAIL_key")
        txtJob.placeholder = mapping.string(forKey: "job_placeholder_key")
        txtPhone.placeholder = mapping.string(forKey: "PHONE_key")
         
        txtvwDescription.textContainerInset = UIEdgeInsets(top: 10, left: 0, bottom: 5, right: 10)
        txtvwDescription.font =  MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        txtvwDescription.delegate = self
        
        btnSend.backgroundColor = MySingleton.sharedManager.themeGreenColor
        btnSend.setTitle(mapping.string(forKey: "SEND_key"), for: .normal)
        
        lbljobName.isHidden = true
        
    }
    
    
}

extension CraftDetailsVc
{
    
    @IBAction func btnCallTapped(_ sender: UIButton)
    {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!)
        {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            } 
            
            callMailWebsiteClickAPI(strStaticType: "0")
        }
        else
        {
           // KSToastView.ks_showToast("Call facility is not available!!!", duration: ToastDuration)
        }
        
    }
    
    
    @IBAction func btnEmailTapped(_ sender: UIButton)
    {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([strEmail])
            
            present(mail, animated: true)
            
            callMailWebsiteClickAPI(strStaticType: "1")
            
        } else {
            // show failure alert
        }
    }
    
    
    
    @IBAction func btnWebsiteTapped(_ sender: UIButton)
    {
        if let link = URL(string: self.lblWebsiteValue.text!) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(link)
            } else {
                UIApplication.shared.openURL(link)
            }
            
            callMailWebsiteClickAPI(strStaticType: "2")
        }
    }
    
    
    func Details(dictDetails : JSON)
    {
        print("dictDetails : \(dictDetails)")
        
        self.fillupData(dictDetails:dictDetails)
        
        if dictDetails["allow_inquiry"].stringValue == "1"
        {
            // self.vwForm.isHidden = false
            [vwFullName,vwEmail,vwPhone,vwDescription,vwJob,vwBtn].forEach({ (vw) in
                vw?.isHidden = false
            })
            
            //heightConstantvwForm.constant = 440
            
            if dictDetails["allow_jobs"].stringValue == "1"
            {
                self.vwJob.isHidden = false
                
                //heightConstantvwForm.constant = 370
                self.strCategoryID = dictDetails["category_id"].stringValue
                self.getJobList()
            }
            else
            {
                //heightConstantvwForm.constant = 0
                self.vwJob.isHidden = true
            }
            
        }
        else
        {
            //self.vwForm.isHidden = true
            [vwFullName,vwEmail,vwPhone,vwDescription,vwJob,vwBtn].forEach({ (vw) in
                vw?.isHidden = true
            })
        }
    }
    
    func fillupData(dictDetails : JSON)
    {
        lblNameValue.text = dictDetails["craftsman_name"].stringValue
        lblEmailValue.text = dictDetails["email"].stringValue
        lblPhoneValue.text = dictDetails["phone_no"].stringValue
        lblLocationValue.text = dictDetails["location"].stringValue
        
        if dictDetails["website"].stringValue == ""
        {
            self.vwWebSite.isHidden = true
        }
        else
        {
            self.vwWebSite.isHidden = false
            self.lblWebsiteValue.text = dictDetails["website"].stringValue
        }
        
        lblDescription.text = dictDetails["description"].stringValue
        
        self.strCraftManID = dictDetails["id"].stringValue
        print("StrCraftManID : \(self.strCraftManID)")
        self.strPhoneNumber = dictDetails["phone_no"].stringValue
        self.strEmail = dictDetails["email"].stringValue
        
        let url = dictDetails["image"].url
        
        img.sd_setShowActivityIndicatorView(true)
        img.sd_setIndicatorStyle(.gray)
        
        img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_list"), options: .lowPriority, completed: nil)
        
    }
    
//    func configDD(dropdown: DropDown, sender: UIView)
//    {
//        dropdown.anchorView = sender
//        dropdown.direction = .any
//        dropdown.dismissMode = .onTap
//        dropdown.multiSelectionAction
//        dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
//        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
//        print("textfield Frame \(txtPurpose.frame)")
//        dropdown.width = sender.bounds.width
//        dropdown.cellHeight = 40.0
//        dropdown.backgroundColor = UIColor.white
//        dropdown.textColor = MySingleton.sharedManager.themeGreenColor
//        dropdown.selectionBackgroundColor = UIColor.clear
//
//    }
    
//    func selectionIndex()
//    {
//
//        self.jobDD.selectionAction = { (index, item) in
//            self.txtJob.text = item
//            self.strJobID = (self.aryJSONJob[index]["category_id"]).stringValue
//            self.jobDD.hide()
//        }
//
//    }
    
    @IBAction func btnSendTapped(_ sender: UIButton)
    {
        if (txtFullName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            
            makeToast(strMessage: mapping.string(forKey: "Please_enter_full_name_key"))
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            
            makeToast(strMessage: mapping.string(forKey: "Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            
            makeToast(strMessage: mapping.string(forKey: "Please_enter_valid_email_key"))
        }
        else if (txtPhone.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            
            makeToast(strMessage: mapping.string(forKey: "Please_enter_Phone_number_key"))
        }
        else if (txtPhone.text?.characters.count)! < 10
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_valid_phone_number_key"))
        }
        else if (txtvwDescription.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            
            makeToast(strMessage: mapping.string(forKey: "Please_enter_description_key"))
        }
        else
        {
            if dictDetails["allow_jobs"].stringValue == "1"
            {
                if (self.lbljobName.text?.isEmpty)!
                {
                    
                    makeToast(strMessage: mapping.string(forKey: "Please_select_job_key"))
                    return
                }
                else
                {
                   self.sendDetailsAPI()
                }
            }
            else
            {
                 self.sendDetailsAPI()
            }
            
        }
    }
    
    
    //MARK: - JOB list API
    
    func getJobList()
    {
        
            if (Alamofire.NetworkReachabilityManager()?.isReachable)!
            {
                
                let url = "\(userURL)\(jobsList)"
                
                print("URL: \(url)")
                
                let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                             "category_id" : self.strCategoryID,
                             "craftman_id" : self.strCraftManID
                ]
                
                print("param :\(param)")
                
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                linearBar?.stopAnimation()
                self.lineBar.isHidden = true
                    
                    
                    if let json = respones.value
                    {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == "1"
                        {
                            
                            //self.arrayJob = []

                            let ary = json["data"].arrayValue
                            
                            self.aryJSONJob = []

                           // self.aryJSONJob = dict.arrayValue

                            for i in 0..<ary.count
                            {
                                var dict = ary[i]
                                dict["selected"] = "0"
                                self.aryJSONJob.append(dict)

                            }
                          //  print("Array : \(self.aryJSONJob)")
//                            self.jobDD.dataSource = self.arrayJob as! [String]
                            
                        }
                        else
                        {
                            makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                        }
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                }
            }
            else
            {
                makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
            }
        
    }
    
    //MARK: - Send Details API
    //TODO: - Version 1
    
    /*
    func sendDetailsAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            vwForm.isUserInteractionEnabled = false
            
            let url = "\(userURL)\(request_inquiry)"
            
            print("URL: \(url)")
            
            var param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "craftsman_id" : self.strCraftManID,
                         "full_name" : self.txtFullName.text ?? "" ,
                         "email" : self.txtEmail.text ?? "",
                         "phone_number" : self.txtPhone.text ?? "",
                         "description" : self.txtvwDescription.text ?? ""
            ]
            
            if dictDetails["allow_jobs"].stringValue == "1"
            {
                param["job_id"] = self.strJobID
            }
            else
            {
                param["job_id"] = "0"
            }
            
            print("param :\(param)")
            
            
            self.lineBar.isHidden = false
            linearBar?.startAnimation()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                linearBar?.stopAnimation()
                self.lineBar.isHidden = true
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        makeToast(strMessage: "\(json["msg"].stringValue)")
                        self.txtFullName.text = ""
                        self.txtEmail.text = ""
                        self.txtPhone.text = ""
                        self.txtvwDescription.text = ""
                        self.lblPlaceholderDescription.isHidden = false
                        self.txtJob.text = ""
                        self.lbljobName.isHidden = true
                        self.txtJob.placeholder = mapping.string(forKey: "job_placeholder_key")
                        
                        for i in 0..<self.aryJSONJob.count
                        {
                            var dict = self.aryJSONJob[i]
                            dict["selected"] = "0"
                            self.aryJSONJob[i] = dict
                        }
                        
                        print("aryJSONJob:\(self.aryJSONJob)")
                        
                        self.vwForm.isUserInteractionEnabled = true
                        
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    */
    
    //TODO: - Version 2
    
    func sendDetailsAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            vwForm.isUserInteractionEnabled = false
            
            let url = "\(userURL)\(requestInquiryV2)"
            
            print("URL: \(url)")
            
            var param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "craftsman_id" : self.strCraftManID,
                         "full_name" : self.txtFullName.text ?? "" ,
                         "email" : self.txtEmail.text ?? "",
                         "phone_number" : self.txtPhone.text ?? "",
                         "description" : self.txtvwDescription.text ?? "",
                         "user_id" : getSetLocation("user_id"),
                         "access_token" : getSetLocation("access_token")
            ]
            
            if dictDetails["allow_jobs"].stringValue == "1"
            {
                param["job_id"] = self.strJobID
            }
            else
            {
                param["job_id"] = "0"
            }
            
            print("param :\(param)")
            
            
            self.lineBar.isHidden = false
            linearBar?.startAnimation()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                linearBar?.stopAnimation()
                self.lineBar.isHidden = true
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        makeToast(strMessage: "\(json["msg"].stringValue)")
                        self.txtFullName.text = ""
                        self.txtEmail.text = ""
                        self.txtPhone.text = ""
                        self.txtvwDescription.text = ""
                        self.lblPlaceholderDescription.isHidden = false
                        self.txtJob.text = ""
                        self.lbljobName.isHidden = true
                        self.txtJob.placeholder = mapping.string(forKey: "job_placeholder_key")
                        
                        for i in 0..<self.aryJSONJob.count
                        {
                            var dict = self.aryJSONJob[i]
                            dict["selected"] = "0"
                            self.aryJSONJob[i] = dict
                        }
                        
                        print("aryJSONJob:\(self.aryJSONJob)")
                        
                        self.vwForm.isUserInteractionEnabled = true
                        
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
    //MARK: - CraftMan Details API
    
    //TODO: - Version 1
    
    /*
    func craftMainDetailsAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(craftsman_details)"
            
            print("URL: \(url)")
            
            var param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "craftman_id" : self.strCraftManID
                
            ]
            
            print("param :\(param)")
            
            
            self.lineBar.isHidden = false
            linearBar?.startAnimation()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                linearBar?.stopAnimation()
                self.lineBar.isHidden = true
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        
                        self.Details(dictDetails: data)
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    */
    
    
    //TODO: - Verion 2
    
    func craftMainDetailsAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(craftManDetailsV2)"
            
            print("URL: \(url)")
            
            var param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "craftman_id" : self.strCraftManID,
                         "user_id" : getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token")
                
            ]
            
            print("param :\(param)")
            
            
            self.lineBar.isHidden = false
            linearBar?.startAnimation()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                linearBar?.stopAnimation()
                self.lineBar.isHidden = true
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        
                        self.Details(dictDetails: data)
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
    //MARK: - Call,Mail,Website Api calling
    
    func callMailWebsiteClickAPI(strStaticType : String)
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(manageCraftmanStatics)"
            
            print("URL: \(url)")
            
            var param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "craftman_id" : self.strCraftManID,
                         "user_id" : getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token"),
                         "statistic_type" : strStaticType
                
            ]
            
            print("param :\(param)")
            
            
//            self.lineBar.isHidden = false
//            linearBar?.startAnimation()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                linearBar?.stopAnimation()
//                self.lineBar.isHidden = true
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
}

extension CraftDetailsVc : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtJob
        {
            if aryJSONJob.count == 0
            {
                makeToast(strMessage: mapping.string(forKey: "sorry_No_any_job_available_key"))
                return false
            }
            else
            {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "jobMultipleSelectionPopup") as! jobMultipleSelectionPopup
                obj.popUpDelegate = self
                obj.arrayJob = aryJSONJob
                
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
                
                return false
            }
            
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtPhone
        {
            let maxLength = 10
            let currentString: NSString = txtPhone.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
}



extension CraftDetailsVc : UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if textView.text == ""
        {
            self.lblPlaceholderDescription.isHidden = false
        }
        else
        {
            self.lblPlaceholderDescription.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            txtvwDescription.resignFirstResponder()
            return false
        }
        return true
    }

}

extension CraftDetailsVc : MFMailComposeViewControllerDelegate
{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension CraftDetailsVc : CommonPopupDelegate
{
    func setPopName(Id: [String], value: [String], arrSelected: [JSON]) {
        print("id \(Id)")
        print("value \(value)")
        self.txtJob.placeholder = ""
        strJobID = Id.joined(separator: ",")
        strJobName = value.joined(separator: ",")
        print("strJobName : \(strJobName)")
        self.lbljobName.isHidden = false
        self.lbljobName.text = strJobName
        self.aryJSONJob = arrSelected
    }
    
    
}
