//
//  CraftListVc.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import SDWebImage
import LinearProgressBarMaterial

class CraftListVc: UIViewController
{
    //MARK: - outlet
    
    
    @IBOutlet weak var lineBar: UIView!
    
    @IBOutlet weak var vwSearch: UIView!
    
    @IBOutlet weak var txtSearch: CustomTextField!
    
    @IBOutlet weak var vwSearchHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var tblCraftList: UITableView!
    
    
    //MARK: - Variable
    
    var strHeaderName = ""
    var strCategoryID = ""
    
    var refreshControl = UIRefreshControl()
    var uperRefreshControl = UIRefreshControl()
    var arrayCraftArray : [JSON] = []
    var strCraftMsg = ""
    var strCraftOffset = ""
    var isLastCell = Bool()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        self.view.addSubview(linearBar!)
        lineBar.isHidden = true
        
        configureLinearProgressBar()
        
        let optionbtn = UIBarButtonItem(image: UIImage(named: "ic_more_header_white"), style: .plain, target: self, action: #selector(btnOptionAction))
        optionbtn.tintColor = .white
        
        let searchbtn = UIBarButtonItem(image: UIImage(named: "ic_search_header_white"), style: .plain, target: self, action: #selector(btnSearchTapped))
        searchbtn.tintColor = .white
        
        self.navigationItem.rightBarButtonItems = [optionbtn,searchbtn]
        
//        self.navigationItem.rightBarButtonItem = rightButton
        
        setupNavigationbarwithBackButton(titleText: strHeaderName)
        
        tblCraftList.register(UINib(nibName: "CraftListTableCell", bundle: nil), forCellReuseIdentifier: "CraftListTableCell")
        
        refreshControl.addTarget(self, action: #selector(self.refreshTableview), for: .valueChanged)
        
        tblCraftList.bottomRefreshControl = refreshControl

        uperRefreshControl.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblCraftList.addSubview(uperRefreshControl)
        
        self.strCraftOffset = "0"
        self.getCraftListAPI(isShowLoader: true)
        
        // Do any additional setup after loading the view.
    }
    
}

extension CraftListVc
{
    
    @objc func btnOptionAction()
    {
        let obj = storyboard?.instantiateViewController(withIdentifier: "PopupVc") as! PopupVc
        obj.popupDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
        
    }
    
    @objc func btnSearchTapped()
    {
        
        let searchVc = storyboard?.instantiateViewController(withIdentifier: "SearchVc") as! SearchVc
        
        searchVc.isFromHome = "1"
        searchVc.strCategoryID = self.strCategoryID
        
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
    @objc func upperRefreshTable()
    {
        self.strCraftOffset = "0"
        uperRefreshControl.beginRefreshing()
        getCraftListAPI(isShowLoader: false)
    }
    
    @objc func refreshTableview()
    {
        if isLastCell == true
        {
            print("offset : \(strCraftOffset)")
            if strCraftOffset != "-1" && strCraftOffset != "0"
            {
                self.isLastCell = false
                refreshControl.beginRefreshing()
                getCraftListAPI(isShowLoader: false)
            }
            else
            {
                refreshControl.endRefreshing()
            }
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    
    //TODO: - Version1
   /*
    func getCraftListAPI(isShowLoader:Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(craftList)"
            
            print("URL: \(url)")
            
            if self.strCraftOffset == "-1"
            {
                refreshControl.endRefreshing()
                uperRefreshControl.endRefreshing()
                return
            }
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "category_id" : self.strCategoryID,
                         "offset" : self.strCraftOffset
                        ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if isShowLoader
                {
                    linearBar?.stopAnimation()
                }
                self.lineBar.isHidden = true
                
                self.refreshControl.endRefreshing()
                
                self.uperRefreshControl.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        if self.strCraftOffset == "0"
                        {
                            self.arrayCraftArray = []
                        }
                        
                        self.strCraftOffset = json["next_offset"].stringValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayCraftArray + aryData
                        self.arrayCraftArray = aryData
                        
                    }
                    else
                    {
                        self.arrayCraftArray = []
                        self.strCraftMsg = json["msg"].stringValue
                        
                    }
                    self.tblCraftList.reloadData()
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    */
    
    
    //TODO: - Version 2
    
    func getCraftListAPI(isShowLoader:Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(craftListV2)"
            
            print("URL: \(url)")
            
            if self.strCraftOffset == "-1"
            {
                refreshControl.endRefreshing()
                uperRefreshControl.endRefreshing()
                return
            }
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "category_id" : self.strCategoryID,
                         "offset" : self.strCraftOffset,
                         "user_id" : getSetLocation("user_id"),
                         "access_token" : getSetLocation("access_token")
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if isShowLoader
                {
                    linearBar?.stopAnimation()
                }
                self.lineBar.isHidden = true
                
                self.refreshControl.endRefreshing()
                
                self.uperRefreshControl.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        if self.strCraftOffset == "0"
                        {
                            self.arrayCraftArray = []
                        }
                        
                        self.strCraftOffset = json["next_offset"].stringValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayCraftArray + aryData
                        self.arrayCraftArray = aryData
                        
                    }
                    else
                    {
                        self.arrayCraftArray = []
                        self.strCraftMsg = json["msg"].stringValue
                        
                    }
                    self.tblCraftList.reloadData()
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    
}



extension CraftListVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (arrayCraftArray.count == 0)
        {
            let lbl = UILabel()
            lbl.text = strCraftMsg
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeGreenColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return arrayCraftArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tblCraftList.dequeueReusableCell(withIdentifier: "CraftListTableCell", for: indexPath) as! CraftListTableCell
        
        let data = self.arrayCraftArray[indexPath.row]
        
        cell.lblCraftName.text = data["craftsman_name"].stringValue
        
        let url = data["image"].url
        
        cell.imgCraft.sd_setShowActivityIndicatorView(true)
        cell.imgCraft.sd_setIndicatorStyle(.gray)
        
        cell.imgCraft.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_list"), options: .lowPriority, completed: nil)
        
        let totalRow = tableView.numberOfRows(inSection: indexPath.section)
        //first get total rows in that section by current indexPath.
        if indexPath.row == totalRow - 1 {
            //this is the last row in section.
            isLastCell = true
            self.refreshTableview()
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let details = storyboard?.instantiateViewController(withIdentifier: "CraftDetailsVc") as! CraftDetailsVc
        details.strCategoryID = self.strCategoryID
        details.dictDetails = self.arrayCraftArray[indexPath.row]
        self.navigationController?.pushViewController(details, animated: true)
    }
    
}

extension CraftListVc : popupCommonDelegate
{
    func setLocationMap()
    {
        
        let MapViewController = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        MapViewController.isFirstTimeLocationSet = "1"
        self.navigationController?.pushViewController(MapViewController, animated: true)
        
    }
    
    func sharedAction()
    {
        
        let strMsg = "Hey check out my app at: "
        let link = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
//        UIGraphicsEndImageContext()
        let activityVC = UIActivityViewController(activityItems: [strMsg,link], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    func rateusInAppStore()
    {
        print("Rate Now")
        
        //   let url = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
        
        let openAppStoreForRating = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
        
        let url = URL(string: openAppStoreForRating)!
        if UIApplication.shared.canOpenURL(URL(string: openAppStoreForRating)!) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
            
        } else {
            makeToast(strMessage: "Please select our app from the AppStore and write a review for us. Thanks!!")
            
        }
        
    }
    
/*    func searchActionForHeight()
    {
//        UIView.animate(withDuration: 0.3, animations: {
//            //  self.view.alpha = 1.0
//
//            self.vwSearch.isHidden = false
//            self.vwSearchHeightConstant.constant = 50
//            self.view.layoutIfNeeded()
//
//        })
        
        let searchVc = storyboard?.instantiateViewController(withIdentifier: "SearchVc") as! SearchVc
        
        searchVc.isFromHome = "1"
        searchVc.strCategoryID = self.strCategoryID
        
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
 */
    
    func NotificationAction()
    {
        let notification = storyboard?.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
        
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
}
