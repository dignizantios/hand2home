//
//  HomeVc.swift
//  Hand2Home
//
//  Created by YASH on 26/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import SDWebImage
import LinearProgressBarMaterial
import Firebase

class HomeVc: UIViewController
{
    
    //MARK: - Outlet
    @IBOutlet weak var vwSearch: UIView!
    
    @IBOutlet weak var vwSearchHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: CustomTextField!
    
    @IBOutlet weak var collectionVw: UICollectionView!
    
    @IBOutlet weak var lineBar: UIView!
    
    //MARK: - Variable
    
    var arrayCategory : [JSON] = []
    var strCategoryMsg = ""
    
    var refreshControl = UIRefreshControl()
    
    
    //MARK: - View Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: 5)
        self.view.addSubview(linearBar!)
        lineBar.isHidden = true
        
        configureLinearProgressBar()
    
        let optionbtn = UIBarButtonItem(image: UIImage(named: "ic_more_header_white"), style: .plain, target: self, action: #selector(btnOptionAction))
        optionbtn.tintColor = .white
        
        let searchbtn = UIBarButtonItem(image: UIImage(named: "ic_search_header_white"), style: .plain, target: self, action: #selector(btnSearchTapped))
        searchbtn.tintColor = .white
        
        self.navigationItem.rightBarButtonItems = [optionbtn,searchbtn]
        self.navigationItem.hidesBackButton = true
//        self.navigationItem.rightBarButtonItem = optionbtn
        
        setupNavigationBar(titleText: mapping.string(forKey: "Category_key"))
    
        collectionVw.register(UINib(nibName: "HomeCollectionCell", bundle: nil), forCellWithReuseIdentifier:"HomeCollectionCell")
        collectionVw.alwaysBounceVertical = true
        refreshControl.addTarget(self, action: #selector(refreshCollection), for: .valueChanged)
        
        collectionVw.addSubview(refreshControl)
        
        
        //self.perform(#selector(callapi), with: nil, afterDelay: 5.0)
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(RedirectToDetails), name: NSNotification.Name(rawValue: "RedirectToDetails"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReloadCollection), name: NSNotification.Name(rawValue: "ReloadColection"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(grantedFalse), name: NSNotification.Name(rawValue: "grantedFalse"), object: nil)
        
        self.listAPI(isShowLoader: true)
        
    }
    
    @objc func grantedFalse()
    {
        listAPI(isShowLoader: true)
    }
    
    @objc func ReloadCollection(_ notification : NSNotification)
    {
        if let object = notification.object
        {
            self.arrayCategory = []
            self.arrayCategory = object as! [JSON]
            print("arrayCategory : \(arrayCategory)")
            
            self.collectionVw.reloadData()
        }
    }

}


extension HomeVc
{
    @objc func RedirectToDetails(_ notification : NSNotification)
    {
        if let object = notification.object
        {
            print("object : \(object)")
            let navToDetails = self.storyboard?.instantiateViewController(withIdentifier: "CraftDetailsVc") as! CraftDetailsVc
            
            navToDetails.isFromNotificationList = "0"
            navToDetails.strCraftManID = object as! String
            
            self.navigationController?.pushViewController(navToDetails, animated: true)
            
        }
        
    }
    
    
    @objc func refreshCollection()
    {
       
        self.refreshControl.beginRefreshing()
        self.listAPI(isShowLoader: false)
    }
    
  /*
     //TODO: - version 1
    func listAPI(isShowLoader:Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
        let url = "\(userURL)\(categoryList)"
        
        print("URL: \(url)")
        
        let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                     "register_id" : Messaging.messaging().fcmToken ?? "",
                     "device_type" : "1"]
        
        print("param :\(param)")
            
            
            if isShowLoader == true
            {
                 self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
        
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if isShowLoader == true
            {
                linearBar?.stopAnimation()
            }
            
            self.lineBar.isHidden = true
            self.refreshControl.endRefreshing()
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == "1"
                {
                    self.arrayCategory = []
                    
                    var aryData = json["data"].arrayValue
                    aryData = self.arrayCategory + aryData
                    self.arrayCategory = aryData
                    self.collectionVw.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: "\(json["msg"].stringValue)")
                }
            }
            else
            {
                makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
            }
          }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    
    */
    
    //TODO: - version 2
    
    func listAPI(isShowLoader:Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(userURL)\(categoryListV2)"
            
            print("URL: \(url)")
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "user_id" : getSetLocation("user_id"),
                         "access_token" : getSetLocation("access_token")]
            
            print("param :\(param)")
            
            if isShowLoader == true
            {
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if isShowLoader == true
                {
                    linearBar?.stopAnimation()
                }
                
                self.lineBar.isHidden = true
                self.refreshControl.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        self.arrayCategory = []
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayCategory + aryData
                        self.arrayCategory = aryData
                        self.collectionVw.reloadData()
                    }
                    else
                    {
                        makeToast(strMessage: "\(json["msg"].stringValue)")
                    }
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    
    
    
    
    @objc func btnOptionAction()
    {
        let obj = storyboard?.instantiateViewController(withIdentifier: "PopupVc") as! PopupVc
        obj.popupDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
    
    }
    
    @objc func btnSearchTapped()
    {
        
        let searchVc = storyboard?.instantiateViewController(withIdentifier: "SearchVc") as! SearchVc
        
        searchVc.isFromHome = "0"
        
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
    
    //MARK: - Setting for Location
    /*
    
    */
}


extension HomeVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayCategory.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
        
        let data = self.arrayCategory[indexPath.row]
        cell.lblName.text = data["category"].stringValue
        
        cell.img.sd_setShowActivityIndicatorView(true)
        cell.img.sd_setIndicatorStyle(.gray)
        
        let url = data["category_image"].url
        
        cell.img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_category_screen"), options: .lowPriority, completed: nil)
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let craftvc = self.storyboard?.instantiateViewController(withIdentifier: "CraftListVc") as! CraftListVc
        
        let data = self.arrayCategory[indexPath.row]
        craftvc.strHeaderName = data["category"].stringValue
        craftvc.strCategoryID = data["id"].stringValue
        
        self.navigationController?.pushViewController(craftvc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let width = (collectionView.frame.size.width/3)
       // let height = (collectionView.frame.size.height/4)
        
        return CGSize(width: width , height:width )
    }
}


//extension Homevc : LocationDelegate
//{
//    func didUpdateLocation(lat: Double?, lon: Double?) {
//
//        print("Lat : \(lat)")
//        print("Lon : \(lon)")
//    }
//
//    func didFailedLocation(error: String) {
//        print("Error : \(error)")
//    }
//}

extension HomeVc : popupCommonDelegate
{
    func setLocationMap()
    {
        
        let MapViewController = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        MapViewController.isFirstTimeLocationSet = "1"
        self.navigationController?.pushViewController(MapViewController, animated: true)

    }
    
    func sharedAction() {
        let strMsg = "Hey check out my app at: "
        let link = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
        
//        UIGraphicsEndImageContext()
        let activityVC = UIActivityViewController(activityItems: [strMsg,link], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    
    func rateusInAppStore()
    {
        print("Rate Now")
        
        //   let url = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
        
        let openAppStoreForRating = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
        
        let url = URL(string: openAppStoreForRating)!
        if UIApplication.shared.canOpenURL(URL(string: openAppStoreForRating)!) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
            
        } else {
            
          makeToast(strMessage: "Please select our app from the AppStore and write a review for us. Thanks!!")
        }
        
    }
/*
    func searchActionForHeight()
    {
        print("SearchHeight")
        
//        UIView.animate(withDuration: 0.3, animations: {
//            //  self.view.alpha = 1.0
//
//            self.vwSearch.isHidden = false
//            self.vwSearchHeightConstant.constant = 50
//
//            self.view.layoutIfNeeded()
//
//        })
        
        
        let searchVc = storyboard?.instantiateViewController(withIdentifier: "SearchVc") as! SearchVc
        
        searchVc.isFromHome = "0"
        
        self.navigationController?.pushViewController(searchVc, animated: true)
        
 
    }
 */
    
    func NotificationAction()
    {
        let notification = storyboard?.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
        
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
}
