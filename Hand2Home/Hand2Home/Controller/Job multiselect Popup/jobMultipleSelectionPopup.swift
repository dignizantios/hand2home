//
//  jobMultipleSelectionPopup.swift
//  Hand2Home
//
//  Created by YASH on 07/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

public protocol CommonPopupDelegate
{
    func setPopName(Id:[String],value:[String],arrSelected:[JSON])
}

class jobMultipleSelectionPopup: UIViewController {
    
    //MARK: - outlet
    
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var tblJobList: UITableView!
    
    @IBOutlet weak var heightOfOuterCommon: NSLayoutConstraint!
    
    @IBOutlet weak var heightOftblCommon: NSLayoutConstraint!
    
    @IBOutlet weak var btnDone: UIButton!
    //MARK: - Variable
    
    var arrayJob : [JSON] = []
    
    var popUpDelegate : CommonPopupDelegate?
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblJobList.register(UINib(nibName: "JobMultiSelectCell", bundle: nil), forCellReuseIdentifier: "JobMultiSelectCell")
        
//        print("Aray JOB : \(arrayJob)")
        
        lblHeader.text = mapping.string(forKey: "JOB_key")
        
        btnDone.setTitle(mapping.string(forKey: "Done_key"), for: .normal)
        
        self.tblJobList.tableFooterView = UIView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tblJobList.reloadData()
        tblJobList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
        tblJobList.removeObserver(self, forKeyPath: "contentSize")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblJobList.contentSize.height)")
            self.heightOftblCommon.constant = tblJobList.contentSize.height
            let finalHeight = self.heightOftblCommon.constant + 100
            print("finalHeight:= \(finalHeight)")
            if finalHeight > (UIScreen.main.bounds.size.width*428)/320
            {
                self.heightOfOuterCommon.constant = (UIScreen.main.bounds.size.width*428)/320
                self.tblJobList.isScrollEnabled = true
            }
            else
            {
                self.heightOfOuterCommon.constant = finalHeight
                self.tblJobList.isScrollEnabled = false
            }
        }
    }
    
    @IBAction func btnClosePopupTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func btnDonePopupTapped(_ sender: UIButton)
    {
        var strId:[String] = [String()]
        var strName:[String] = [String()]
        for i in 0..<arrayJob.count
        {
            let dict = arrayJob[i]
            if dict["selected"].stringValue == "1"
            {
                strId.append(dict["id"].stringValue)
                strName.append(dict["job_name"].stringValue)
            }
        }
        strId.remove(at: 0)
        strName.remove(at: 0)
        if strId.count == 0
        {
            makeToast(strMessage: mapping.string(forKey: "Please_Select_At_Least_One_Job_key"))
            return
        }
        popUpDelegate?.setPopName(Id: strId, value: strName,arrSelected: arrayJob)

        self.dismiss(animated: true, completion: nil)
    }
    

}

extension jobMultipleSelectionPopup : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayJob.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblJobList.dequeueReusableCell(withIdentifier: "JobMultiSelectCell") as! JobMultiSelectCell
        
        let dict = arrayJob[indexPath.row]
        
        cell.lblJob.text = dict["job_name"].stringValue
        
        
        if dict["selected"].stringValue == "1"
        {
            cell.imgSelection.image = #imageLiteral(resourceName: "checkbox_checked")
        }
        else
        {
            cell.imgSelection.image = #imageLiteral(resourceName: "checkbox_unchecked")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dict = arrayJob[indexPath.row]
        print("dict :\(dict)")
        if dict["selected"].stringValue == "0"
        {
            dict["selected"].stringValue = "1"
            arrayJob[indexPath.row] = dict
        }
        else
        {
            dict["selected"].stringValue = "0"
            arrayJob[indexPath.row] = dict
        }
        
        self.tblJobList.reloadData()
    }
}
