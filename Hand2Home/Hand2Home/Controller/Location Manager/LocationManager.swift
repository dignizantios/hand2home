//
//  LocationManager.swift
//
//
//  Created by Haresh Vavadiya on 5/15/17.
//  Copyright © 2017 Jaydeep Vora. All rights reserved.
//

import UIKit
import CoreLocation
import Toaster
protocol LocationDelegate {
    func didUpdateLocation(lat:Double?,lon:Double?)
    func didFailedLocation(error:String)
    
}

class Location: NSObject,CLLocationManagerDelegate {
    
    static let shared = Location()
    var needToDisplayAlert = false
    var delegate:LocationDelegate!
    var lattitude:Double?
    var longitude:Double?
    
    private var locationManager:CLLocationManager!
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
           // locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
        lattitude = 0.00
        longitude = 0.00
        
    }
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            print("Location didChangeAuthorization to authorizedAlways")
            startUpdatingLocation()
        }
        else if status == .authorizedWhenInUse {
            print("Location didChangeAuthorization to authorizedWhenInUse")
            startUpdatingLocation()
        }
        else if status == .denied {
            if needToDisplayAlert {
                displayAlert()
            }
            print("Location didChangeAuthorization to denied")
            stopUpdatingLocation()
            //openSetting()
            delegate.didFailedLocation(error: "Location service is denied")
        }
        else if status == .notDetermined {
            print("Location didChangeAuthorization to notDetermined")
            startUpdatingLocation()
           // openSetting()
            delegate.didFailedLocation(error: "Location service is not determined")
        }
        else if status == .restricted {
            if needToDisplayAlert {
                displayAlert()
            }
            print("Location didChangeAuthorization to authorizedWhenInUse")
            stopUpdatingLocation()
           // openSetting()
            delegate.didFailedLocation(error: "Location service is restricted")
        }
        else {
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let latestLocation = locations.first {
            userLocation = latestLocation
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
           
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                self.lattitude = latestLocation.coordinate.latitude
                self.longitude = latestLocation.coordinate.longitude
                self.delegate.didUpdateLocation(lat:self.lattitude,lon:self.longitude)
            }
            stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      //  SalonLog("LocationFailedError:-\(error.localizedDescription)")
        //KSToastView.ks_showToast("LocationFailedError:-\(error.localizedDescription)", delay: ToastDuration)
        delegate.didFailedLocation(error: "LocationDidnotAllowError")
        
    }
    
    deinit {
        locationManager = nil
        delegate = nil
    }
    
    func openSetting()
    {
        let alertController = UIAlertController (title: mapping.string(forKey: "Hand2Home_key"), message: mapping.string(forKey: "Please_allow_location_key") , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: mapping.string(forKey: "Setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
//        let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
//        alertController.addAction(cancelAction)
        
        appdelgate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func displayAlert()
    {
        
//        KSToastView.ks_showToast(mapping.string(forKey : "Location_allow_key"), delay: ToastDuration)
        /*let alertController = UIAlertController(title: kTitle, message: , preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)*/
    }
    
}
