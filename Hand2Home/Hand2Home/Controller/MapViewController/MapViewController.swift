//
//  MapViewController.swift
//  Hand2Home
//
//  Created by YASH on 21/03/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import Firebase
import MBProgressHUD
import DropDown

class MapViewController: UIViewController,GMSMapViewDelegate {

    //MARK: - outlet
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var vwGoogleMap: GMSMapView!
   
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var lblSetLocationValue: UILabel!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblGreen: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK: - Variable
    
    var selectedLocation = CLLocationCoordinate2D()
    
    var locationManager: Location!
    
    var locationJSON = JSON()
    
    var placeID = String()
    
    var isFirstTimeLocationSet = String()
    
    var JSONarrayLocaion:[JSON] = []
    var locationDropDown = DropDown()
    var arrayLocation = NSMutableArray()
    var strLocationID = String()
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let launchBefore = UserDefaults.standard.bool(forKey: "launchBefore")
        print("LaunchBefore : \(launchBefore)")
        
        print("isFirstTimeLocationSet : \(isFirstTimeLocationSet)")
        if isFirstTimeLocationSet == "1"
        {
            setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "SetLocation_key"))
            
//            getLocationList()
            self.getLocationAPI()
        }
        else
        {
            if launchBefore
            {
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVc") as! HomeVc
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            else
            {
                setupNavigationBar(titleText: mapping.string(forKey: "Select_Location_key"))
                
//                getLocationList()
                
            }
        }
        
        getLocationList()
        
        
        self.setupUI()
        
        vwGoogleMap.delegate = self
        
//        locationManager = Location()
//        locationManager.needToDisplayAlert = true
//        locationManager.delegate = self
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.locationDropDown, sender: self.txtLocation)
        selectionIndex()
    }
    
    func setupUI()
    {
        
        lblLocation.text = mapping.string(forKey: "Select_Location_key")
        lblLocation.textColor = MySingleton.sharedManager.themeGreenColor
        lblLocation.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        
        txtLocation.placeholder = mapping.string(forKey: "SELECT_HERE_key")
        txtLocation.delegate = self
        
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 50, height: txtLocation.frame.height))
        txtLocation.rightView = paddingView
        txtLocation.rightViewMode = .always
        
        
        btnDone.backgroundColor = MySingleton.sharedManager.themeGreenColor
        btnDone.setTitle(mapping.string(forKey: "Done_key"), for: .normal)
        
    }
    
    
}


extension MapViewController
{
    
    //MARK: - setup Drop down
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = MySingleton.sharedManager.themeLightGrayColor
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    func selectionIndex()
    {
        
        self.locationDropDown.selectionAction = { (index, item) in
            self.txtLocation.text = item
            self.strLocationID = (self.JSONarrayLocaion[index]["id"]).stringValue
            print("strLocationID : \(self.strLocationID)")
            self.locationDropDown.hide()
            
            if self.isFirstTimeLocationSet == "1"
            {
                
            }
            else
            {
                self.perform(#selector(self.interval), with: nil, afterDelay: 10.0)
                
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
        }
        
    }
    
    @objc func interval()
    {
        print("Wait a 5 second")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    //MARK: - Setpin
    
 /*   func setUpPinOnMap(lat:Double,long:Double)
    {
        self.vwGoogleMap.clear()
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "pin_map_green")
        marker.map = vwGoogleMap
        marker.isDraggable = true
//        marker.userData = dict
//        arrMarks.append(marker)
        self.vwGoogleMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 14)
    }
 */
    //MARK: - IBAction method

    /*
    @IBAction func btnBackTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnClearTapped(_ sender: UIButton)
    {
//        let acController = GMSAutocompleteViewController()
//        acController.delegate = self
//        present(acController, animated: false, completion: nil)
        
    }
    */
    
    @IBAction func btnDoneTapped(_ sender: UIButton)
    {
        if (self.txtLocation.text?.isEmpty)!
        {
            makeToast(strMessage: mapping.string(forKey: "Please_select_location_key"))
            return
        }
        else
        {
            self.passLocationDetails()
        }
        
    }
    
/*
 
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                
                let lines = address.lines as? [String]
                
                currentAddress = (lines?.joined(separator: ",") ?? "")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                self.txtSearch.text = currentAddress
                print("Address - ",currentAddress)
                
                self.locationJSON["country"] = JSON(address.country) ?? ""
                self.locationJSON["state"] = JSON(address.administrativeArea) ?? ""
                self.locationJSON["city"] = JSON(address.locality) ?? ""
                self.locationJSON["locality"] = JSON(address.locality) ?? ""
                self.locationJSON["location"] = JSON(currentAddress)
                self.locationJSON["lat"].stringValue = String(address.coordinate.latitude)
                self.locationJSON["long"].stringValue = String(address.coordinate.longitude)
                self.locationJSON["place_id"] = JSON(self.placeID)
                
                print("locatioJSON: \(self.locationJSON)")
                
                let camera = GMSCameraPosition.camera(withLatitude: address.coordinate.latitude ?? 0.0,longitude: address.coordinate.longitude ?? 0.0,zoom: 12)
                self.vwGoogleMap.camera = camera
                
                
            }
        }
        return currentAddress
    }
    
    func findAddressFromTheLocation(lat: Double , long : Double, success: @escaping (Bool, String) -> Void) {
        //  let components = URLComponents(string: "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&sensor=true")!
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: "AIzaSyDmEatsuYs_qssR7U3zvavPIGKbdZp4VF4") // use your key using developer dignizant account of dignizantApp appication
        let address = URLQueryItem(name: "latlng", value: "\(lat),\(long)")
        components.queryItems = [key, address]
        print(components.url!)
        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                print(String(describing: response))
                print(String(describing: error))
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                print("not JSON format expected")
                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                return
            }
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" else {
                    print("no results")
                    print(String(describing: json))
                    return
            }
            print(results)
            DispatchQueue.main.async {
                // now do something with the results, e.g. grab `formatted_address`:
                //let address_components = results.flatMap { $0["address_components"] as? [String:Any] }
                
                let strings = results.flatMap { $0["place_id"] as? String }
                if let location = strings as? [String]
                {
                    print(location)
                    //if(location.count > 1)
                    //{
                    //success(true, location[1])
                    //}else{
                    success(true, location[0])
                    //}
                }
                
            }
        }
        task.resume()
    }
 
    */
    
    //MARK: - Set location/update location
    
    func setLocationAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(location)"
            
            print("URL: \(url)")
            
            guard let rowString = self.locationJSON.rawString() else{
                return
            }
            
            print("rowStrin : \(rowString)")
            
            var param : [String:String] = [:]
            
            if isFirstTimeLocationSet == "1"
            {
                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token"),
                         "location" : rowString,
                         "flag_view": "1"
                    
                ]
            }
            else
            {
                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : "",
                         "access_token": "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type": "1",
                         "location_id" : "",
                         "flag_view": "0"
                    
                ]
            }
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        let data = json["data"].arrayValue
                        
                        self.JSONarrayLocaion = data
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
    
    //MARK: - getlocation
    
    
    func getLocationAPI()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(location)"
            
            print("URL: \(url)")
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token"),
                         "flag_view": "0"
                
            ]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        
                        self.txtLocation.text = data["location_sw"].stringValue
                        
                        self.strLocationID = data["location_id"].stringValue
                        
//                        let lat = data["lat"].doubleValue
//                        let long = data["long"].doubleValue
                        
                      //  let str = self.reverseGeoCoding(Latitude: lat, Longitude: long)
                       // print("str : \(str)")
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
    //MARK: - Location list
    
    func getLocationList()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(location)"
            
            print("URL: \(url)")
            
            
            var param : [String:String] = [:]
            
//            if isFirstTimeLocationSet == "1"
//            {
//                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
//                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
//                         "user_id" : getSetLocation("user_id"),
//                         "access_token": getSetLocation("access_token"),
//                         "location_id" : getSetLocation("location_id"),
//                         "flag_view": "1"
//
//                ]
//            }
//            else
//            {
                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : "",
                         "access_token": "",
                         "register_id" : "",
                         "device_type": "1",
                         "location_id" : "",
                         "flag_view": "0"
                    
                ]
//            }
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        self.JSONarrayLocaion = []
                        
                        let data = json["data"].arrayValue
                        
                        self.JSONarrayLocaion = data
                        
                        
                        for i in 0..<data.count
                        {
                            let location = data[i]["location_sw"].stringValue
                            self.arrayLocation.add(location)
                        }
                        print("Array : \(self.arrayLocation)")
                        self.locationDropDown.dataSource = self.arrayLocation as! [String]
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
    
    func passLocationDetails()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(location)"
            
            print("URL: \(url)")
            
            
            var param : [String:String] = [:]
            
            if isFirstTimeLocationSet == "1"
            {
                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token"),
                         "location_id" : self.strLocationID,
                         "flag_view": "1"

                ]
            }
            else
            {
                param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         "user_id" : "",
                         "access_token": "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type": "1",
                         "location_id" : self.strLocationID,
                         "flag_view": "1"
                    
                ]
            }
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        self.JSONarrayLocaion = []
                        
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "SetLocation")

                        UserDefaults.standard.set(true, forKey: "launchBefore")
                        UserDefaults.standard.synchronize()
                        
                       
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVc") as! HomeVc
                        
                        self.navigationController?.pushViewController(vc, animated: false)
                        
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                    
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
        
    }
    
}

extension MapViewController: UITextFieldDelegate
{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLocation
        {
            locationDropDown.show()
            return false
            
//            self.navigationController?.isNavigationBarHidden = true
//            let acController = GMSAutocompleteViewController()
//            acController.delegate = self
//            present(acController, animated: false, completion: nil)
//            return false
        }
        return true
    }
    
    
}

/*
extension MapViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place Lat : \(place.coordinate.latitude)")
        print("Place Lon : \(place.coordinate.longitude)")
        
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude ?? 0.0,longitude: place.coordinate.longitude ?? 0.0,zoom: 12)
        vwGoogleMap.camera = camera
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
        
        //reverseGeoCoding(Latitude: place.coordinate.latitude, Longitude: place.coordinate.longitude)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
        print("latitude")
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        
        print("Defalult latitude \(position.target.latitude)")
        print("Defalult longitude \(position.target.longitude)")
        
        selectedLocation.latitude  = position.target.latitude
        selectedLocation.longitude  = position.target.longitude
        
        self.findAddressFromTheLocation(lat: position.target.latitude, long: position.target.longitude) { (Success, placeid) in
            print("placeID after address: \(placeid)")
            self.placeID = placeid
        }
        
        let str = reverseGeoCoding(Latitude: position.target.latitude, Longitude: position.target.longitude)
        self.view.endEditing(true)
        
    }
    
}




extension MapViewController: LocationDelegate
{
    func didUpdateLocation(lat: Double?, lon: Double?) {
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ?? 0.0,longitude: lon ?? 0.0,zoom: 12)
        vwGoogleMap.camera = camera
                
    }
    
    func didFailedLocation(error: String) {
        print("Error : \(error)")
    }
    
}

*/

