//
//  NotificationListVc.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import LinearProgressBarMaterial

class NotificationListVc: UIViewController
{

    //MARK: - Outlet
    
    @IBOutlet weak var tblNotificationList: UITableView!
    
    @IBOutlet weak var lineBar: UIView!
    
    
    //MARK: - Variable
    
    var arrayNotification : [JSON] = []
    var strOffset = ""
    var strMsg = ""
    
    var upperRefreshControl = UIRefreshControl()
    var bottomRefreshControl = UIRefreshControl()
    
    var isLastCell = Bool()
    
    //MARK: - view life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        isNotificationScreen = true
        
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "Notification_key"))

        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        self.view.addSubview(linearBar!)
        lineBar.isHidden = true
        
        configureLinearProgressBar()
        
        tblNotificationList.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        self.tblNotificationList.tableFooterView = UIView()
        
        bottomRefreshControl.addTarget(self, action: #selector(self.refreshTableview), for: .valueChanged)
        tblNotificationList.bottomRefreshControl = bottomRefreshControl
        
        upperRefreshControl.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblNotificationList.addSubview(upperRefreshControl)
        
        self.strOffset = "0"
        self.getNotificationList(isShowLoader: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationUpdate), name: NSNotification.Name(rawValue: "NotificaionUpdated"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isNotificationScreen = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificaionUpdated"), object: nil)
    }
    
   
}

extension NotificationListVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (arrayNotification.count == 0)
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeGreenColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return arrayNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tblNotificationList.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        
        
        let data = self.arrayNotification[indexPath.row]
        
        cell.lblNotification.setHTMLFromString(htmlText: data["message"].stringValue)
        
        let totalRow = tableView.numberOfRows(inSection: indexPath.section)
        //first get total rows in that section by current indexPath.
        if indexPath.row == totalRow - 1 {
            //this is the last row in section.
            isLastCell = true
            self.refreshTableview()
            
        }
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let data = self.arrayNotification[indexPath.row]
        
        if data["type"].stringValue == "1"
        {
            let details = storyboard?.instantiateViewController(withIdentifier: "CraftDetailsVc") as! CraftDetailsVc
            
            details.isFromNotificationList = "0"
            details.strCraftManID = data["craftman_id"].stringValue
            
            self.navigationController?.pushViewController(details, animated: true)
        }
        
    }
    
}


extension NotificationListVc
{

    @objc func NotificationUpdate()
    {
        self.strOffset = "0"
        self.getNotificationList(isShowLoader: true)
    }
    
    //MARK: - Bootom Refresh
    @objc func refreshTableview()
    {
        if isLastCell == true
        {
            
            if strOffset != "-1" && strOffset != "0"
            {
                self.isLastCell = false
                bottomRefreshControl.beginRefreshing()
                getNotificationList(isShowLoader: false)
            }
            else
            {
                bottomRefreshControl.endRefreshing()
            }
        }
        else
        {
            bottomRefreshControl.endRefreshing()
        }
    }
    
    //MARK: - upper Refresh
    @objc func upperRefreshTable()
    {

        self.strOffset = "0"
        upperRefreshControl.beginRefreshing()
        getNotificationList(isShowLoader: false)
       
    }
    
    
    //TODO: - Version 1
    
    /*
    func getNotificationList(isShowLoader : Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(notoficationlist)"
            
            print("URL: \(url)")
            
            if self.strOffset == "-1"
            {
                upperRefreshControl.endRefreshing()
                bottomRefreshControl.endRefreshing()
                return
            }
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "offset" : self.strOffset
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if isShowLoader
                {
                    linearBar?.stopAnimation()
                }
                self.lineBar.isHidden = true
                
                self.upperRefreshControl.endRefreshing()
                
                self.bottomRefreshControl.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        if self.strOffset == "0"
                        {
                            self.arrayNotification = []
                        }
                        
                        self.strOffset = json["next_offset"].stringValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayNotification + aryData
                        self.arrayNotification = aryData
                        
                    }
                    else
                    {
                        self.arrayNotification = []
                        self.strMsg = json["msg"].stringValue
                        
                    }
                    self.tblNotificationList.reloadData()
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    */
    
    
    //TODO: - Version 2
    
    func getNotificationList(isShowLoader : Bool)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(notificationListV2)"
            
            print("URL: \(url)")
            
            if self.strOffset == "-1"
            {
                upperRefreshControl.endRefreshing()
                bottomRefreshControl.endRefreshing()
                return
            }
            
            let param = ["lang" : String(Defaults.value(forKey: "lang") as! Int),
                         "offset" : self.strOffset,
                         "user_id": getSetLocation("user_id"),
                         "access_token": getSetLocation("access_token")
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.lineBar.isHidden = false
                linearBar?.startAnimation()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if isShowLoader
                {
                    linearBar?.stopAnimation()
                }
                self.lineBar.isHidden = true
                
                self.upperRefreshControl.endRefreshing()
                
                self.bottomRefreshControl.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        
                        if self.strOffset == "0"
                        {
                            self.arrayNotification = []
                        }
                        
                        self.strOffset = json["next_offset"].stringValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayNotification + aryData
                        self.arrayNotification = aryData
                        
                    }
                    else
                    {
                        self.arrayNotification = []
                        self.strMsg = json["msg"].stringValue
                        
                    }
                    self.tblNotificationList.reloadData()
                }
                else
                {
                    makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
        }
    }
    
}

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            //print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
