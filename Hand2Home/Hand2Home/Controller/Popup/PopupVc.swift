//
//  PopupVc.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit


protocol popupCommonDelegate  {
    
 //   func searchActionForHeight()
    
    func NotificationAction()
    
    func sharedAction()
    
    func rateusInAppStore()
    
    func setLocationMap()
}


class PopupVc: UIViewController,UIGestureRecognizerDelegate
{
    
    //MARK: - variable
    
    
    @IBOutlet weak var btnNotification: UIButton!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var btnRateNow: UIButton!
    
    @IBOutlet weak var btnShareNow: UIButton!
    
    @IBOutlet weak var vwHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vwOption: UIView!
    
    @IBOutlet weak var btnSetLocation: UIButton!
    
    
    //MARK: - Variable
    
    var popupDelegate : popupCommonDelegate?
    
    var isShare = Bool()

    //MARK: - View life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()
        
        self.vwHeight.constant = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(TappedGesture))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
       
       showAnimate()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        removeAnimate()
        
    }
    
}

extension PopupVc
{
    
    func setupUI()
    {
        
        btnNotification.setTitle(mapping.string(forKey: "Notification_key"), for: .normal)
        btnSearch.setTitle(mapping.string(forKey: "Search_key"), for: .normal)
        btnRateNow.setTitle(mapping.string(forKey: "RateNow_key"), for: .normal)
        btnShareNow.setTitle(mapping.string(forKey: "ShareNow_key"), for: .normal)
        btnSetLocation.setTitle(mapping.string(forKey: "SetLocation_key"), for: .normal)
        
        
        [btnNotification,btnSearch,btnRateNow,btnRateNow,btnSetLocation].forEach { (btn) in
            
            btn?.backgroundColor = .white
            btn?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 19)
            btn?.setTitleColor(MySingleton.sharedManager.themeGreenColor, for: .normal)
            
        }
        
        
    }

    func showAnimate()
    {
        
        UIView.animate(withDuration: 0.5, animations: {
            //  self.view.alpha = 1.0
            self.vwOption.isHidden = false
            self.vwHeight.constant = 200.0
            self.view.layoutIfNeeded()
            
        })
    }
    
    func removeAnimate()
    {
        
        UIView.animate(withDuration: 0.5, animations: {
            //  self.view.alpha = 1.0
            self.vwHeight.constant = 0.0
            
            self.view.layoutIfNeeded()
            self.perform(#selector(self.dismisscontroller), with: nil, afterDelay: 0.3)
            
        })
    }
    
    
    @objc func dismisscontroller()
    {
        
        [self.btnSearch,self.btnRateNow,self.btnShareNow,self.btnNotification,self.btnSetLocation].forEach({ (button) in
            button?.isHidden = true
        })
        
        dismiss(animated: true, completion: nil)
        if isShare == true
        {
            popupDelegate?.sharedAction()
        }
        
    }

    @objc func TappedGesture()
    {
        removeAnimate()
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton)
    {
        print("Notification Tapped")
        
        removeAnimate()
        
        popupDelegate?.NotificationAction()
        
    }
    
//    @IBAction func btnSearchTapped(_ sender: UIButton)
//    {
//        print("Search Tapped")
//
//        removeAnimate()
//
//        popupDelegate?.searchActionForHeight()
//
//
//    }
    
    
    @IBAction func btnRateNowTapped(_ sender: UIButton)
    {
        print("Rate Now")

        removeAnimate()
        
        popupDelegate?.rateusInAppStore()
    }
    
    
    @IBAction func btnShareNowTapped(_ sender: UIButton)
    {
        print("Share Now")
        self.isShare = true
        
        removeAnimate()
        
    }
    
    @IBAction func btnSetLocationTapped(_ sender: UIButton)
    {
        removeAnimate()
        
        popupDelegate?.setLocationMap()
    }
    
}
