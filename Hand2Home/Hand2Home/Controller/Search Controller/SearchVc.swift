//
//  SearchVc.swift
//  Hand2Home
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Toaster
import SDWebImage
import LinearProgressBarMaterial

class SearchVc: UIViewController {

    //MARK: - outlet
    
    @IBOutlet weak var txtSearch: CustomTextField!
    
    @IBOutlet weak var vwSearch: UIView!
    
    @IBOutlet weak var tblSearchList: UITableView!
    
    @IBOutlet weak var lineBar: UIView!
    
    
    //MARK: - Variable
    
    var isFromHome = ""
    /*
     
     isFromHome == 0 (From HomeVc)
     isFromHome == 1 (From CraftList)
 
     */
    
    var isLastCell = Bool()
    var strCategoryID = ""
    var arraySearchList : [JSON] = []
    var strSearchMsg = ""
    var strOffset = ""
    
    var upperRefreshControl = UIRefreshControl()
    var bottomRefreshControl = UIRefreshControl()
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "Search_key"))
        
        tblSearchList.register(UINib(nibName: "SearchListCellTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchListCellTableViewCell")
        
        bottomRefreshControl.addTarget(self, action: #selector(self.refreshTableview), for: .valueChanged)
        
        tblSearchList.bottomRefreshControl = bottomRefreshControl
        
//        upperRefreshControl.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
//        tblSearchList.addSubview(upperRefreshControl)
        
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
    
    
    func setupUI()
    {
        vwSearch.backgroundColor = MySingleton.sharedManager.themeGreenColor
        
        txtSearch.placeholder = mapping.string(forKey: "Search_here_key")
        txtSearch.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        lineBar.addSubview(linearBar!)
        //        self.view.addSubview(linearBar!)
        lineBar.isHidden = true
        
        configureLinearProgressBar()
        
    }

}


extension SearchVc : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if (txtSearch.text?.count)! > 3
//        {
//            self.strOffset = "0"
//            self.arraySearchList = []
//            self.tblSearchList.reloadData()
//            getSearchAPI(isShowLoder: true)
//        }
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        
        if txtSearch.text?.count != 0
        {
            self.strOffset = "0"
//            self.arraySearchList = []
            getSearchAPI(isShowLoder: true)
        }else
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_search_text_first_key"))
        }
    }
}

extension SearchVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (arraySearchList.count == 0)
        {
            let lbl = UILabel()
            lbl.text = strSearchMsg
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeGreenColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return arraySearchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tblSearchList.dequeueReusableCell(withIdentifier: "SearchListCellTableViewCell", for: indexPath) as! SearchListCellTableViewCell
        
        let data = self.arraySearchList[indexPath.row]
        
        if data["search_type"].stringValue == "1"
        {
            cell.lblName.text = data["craftsman_name"].stringValue
            
            let url = data["image"].url
            
            cell.imgCraft.sd_setShowActivityIndicatorView(true)
            cell.imgCraft.sd_setIndicatorStyle(.gray)
            
            cell.imgCraft.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_list"), options: .lowPriority, completed: nil)
        }
        else
        {
            cell.lblName.text = data["category"].stringValue
            
            let url = data["category_image"].url
            
            cell.imgCraft.sd_setShowActivityIndicatorView(true)
            cell.imgCraft.sd_setIndicatorStyle(.gray)
            
            cell.imgCraft.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_list"), options: .lowPriority, completed: nil)
        }
        
        
        let totalRow = tableView.numberOfRows(inSection: indexPath.section)
        //first get total rows in that section by current indexPath.
        if indexPath.row == totalRow - 1 {
            //this is the last row in section.
            isLastCell = true
            self.refreshTableview()
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data = self.arraySearchList[indexPath.row]
        
        if data["search_type"].stringValue == "1"
        {
            let details = storyboard?.instantiateViewController(withIdentifier: "CraftDetailsVc") as! CraftDetailsVc
            
            details.dictDetails = self.arraySearchList[indexPath.row]
            
            self.navigationController?.pushViewController(details, animated: true)
        }
        else
        {
            let details = storyboard?.instantiateViewController(withIdentifier: "CraftListVc") as! CraftListVc
            
            details.strCategoryID = data["id"].stringValue
            details.strHeaderName = data["category"].stringValue
//            details.dictDetails = self.arraySearchList[indexPath.row]
            
            self.navigationController?.pushViewController(details, animated: true)
        }
        
    }
    
}


extension SearchVc
{
    
    
    
    //MARK: - Bootom Refresh
    @objc func refreshTableview()
    {
        if isLastCell == true
        {
            
            if strOffset != "-1" || strOffset == "0"
            {
                self.isLastCell = false
                bottomRefreshControl.beginRefreshing()
                getSearchAPI(isShowLoder: false)
            }
            else
            {
                bottomRefreshControl.endRefreshing()
            }
        }
        else
        {
            bottomRefreshControl.endRefreshing()
        }
    }
    
    //MARK: - upper Refresh
//    @objc func upperRefreshTable()
//    {
//
//        if txtSearch.text?.count != 0
//        {
//            self.strOffset = "0"
//            upperRefreshControl.beginRefreshing()
//            getSearchAPI(isShowLoder: false)
//
//        }else
//        {
//            upperRefreshControl.endRefreshing()
//
//            makeToast(strMessage: mapping.string(forKey: "Please_enter_search_text_first_key"))
//        }
//
//    }
    
    
    //TODO: - Version 1
    /*
    func getSearchAPI(isShowLoder : Bool)
    {
        print("self.strOffset ::: ",self.strOffset)

        if self.strOffset == "-1"
        {
            upperRefreshControl.endRefreshing()
            bottomRefreshControl.endRefreshing()
            return
        }
        
        if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            print("Empty")
            bottomRefreshControl.endRefreshing()
           // makeToast(strMessage: mapping.string(forKey: "Please_enter_full_name_key"))
        }
        else
        {
            if (Alamofire.NetworkReachabilityManager()?.isReachable)!
            {
                let url = "\(userURL)\(searchCraftsman)"
                
                print("URL: \(url)")
                
                var param : [String:String] = [
                    "lang" : String(Defaults.value(forKey: "lang") as! Int),
                    "search_keyword" : self.txtSearch.text ?? "",
                    "offset" : self.strOffset
                ]
                
                if isFromHome == "0"
                {
                    param["search_page"] = "0"
                }
                else
                {
                    param["category_id"] = self.strCategoryID
                    param["search_page"] = "1"
                }
                print("param :\(param)")
                
                self.lineBar.isHidden = false
                
                if isShowLoder == true
                {
                    linearBar?.startAnimation()
                }
                
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                    if isShowLoder
                    {
                        linearBar?.stopAnimation()
                    }
                    self.lineBar.isHidden = true
                    
                    self.bottomRefreshControl.endRefreshing()
                    
                    self.upperRefreshControl.endRefreshing()
                    
                    
                    if let json = respones.value
                    {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == "1"
                        {
                            
                            if self.strOffset == "0"
                            {
                                self.arraySearchList = []
                            }
                            if self.strOffset == "-1"
                            {
                                return
                            }
                            
                            
                            self.strOffset = json["next_offset"].stringValue
                            
                            var aryData = json["data"].arrayValue
                            aryData = self.arraySearchList + aryData
                            self.arraySearchList = aryData
                            
                        }
                        else
                        {
                            self.arraySearchList = []
                            self.strSearchMsg = json["msg"].stringValue
                            
                        }
                        self.tblSearchList.reloadData()
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                }
            }
            else
            {
                makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
            }
            
        }
        
    }
    */
    
    
    //TODO: - Version 2
    
    func getSearchAPI(isShowLoder : Bool)
    {
        print("self.strOffset ::: ",self.strOffset)
        
        if self.strOffset == "-1"
        {
            upperRefreshControl.endRefreshing()
            bottomRefreshControl.endRefreshing()
            return
        }
        
        if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            print("Empty")
            bottomRefreshControl.endRefreshing()
            // makeToast(strMessage: mapping.string(forKey: "Please_enter_full_name_key"))
        }
        else
        {
            if (Alamofire.NetworkReachabilityManager()?.isReachable)!
            {
                let url = "\(userURL)\(searchCraftmanV2)"
                
                print("URL: \(url)")
                
                var param : [String:String] = [
                    "lang" : String(Defaults.value(forKey: "lang") as! Int),
                    "search_keyword" : self.txtSearch.text ?? "",
                    "offset" : self.strOffset,
                    "user_id" : getSetLocation("user_id"),
                    "access_token" : getSetLocation("access_token")
                ]
                
                if isFromHome == "0"
                {
                    param["search_page"] = "0"
                }
                else
                {
                    param["category_id"] = self.strCategoryID
                    param["search_page"] = "1"
                }
                print("param :\(param)")
                
                self.lineBar.isHidden = false
                
                if isShowLoder == true
                {
                    linearBar?.startAnimation()
                }
                
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                    if isShowLoder
                    {
                        linearBar?.stopAnimation()
                    }
                    self.lineBar.isHidden = true
                    
                    self.bottomRefreshControl.endRefreshing()
                    
                    self.upperRefreshControl.endRefreshing()
                    
                    
                    if let json = respones.value
                    {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == "1"
                        {
                            
                            if self.strOffset == "0"
                            {
                                self.arraySearchList = []
                            }
                            if self.strOffset == "-1"
                            {
                                return
                            }
                            
                            
                            self.strOffset = json["next_offset"].stringValue
                            
                            var aryData = json["data"].arrayValue
                            aryData = self.arraySearchList + aryData
                            self.arraySearchList = aryData
                            
                        }
                        else
                        {
                            self.arraySearchList = []
                            self.strSearchMsg = json["msg"].stringValue
                            
                        }
                        self.tblSearchList.reloadData()
                    }
                    else
                    {
                        makeToast(strMessage: mapping.string(forKey: "Something_went_wrong_key"))
                    }
                }
            }
            else
            {
                makeToast(strMessage: "No_internet_connection_please_try_again_later_key")
            }
            
        }
        
    }
    
    
}
