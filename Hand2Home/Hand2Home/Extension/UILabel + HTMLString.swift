//
//  UILabel + HTMLString.swift
//  Hand2Home
//
//  Created by YASH on 06/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation


extension UILabel {
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'Helvetica'; font-size: 16\">%@</span>", htmlText)
        
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html,
                      .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        
        self.attributedText = attrStr
    }
}
