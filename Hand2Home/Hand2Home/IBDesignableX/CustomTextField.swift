//
//  CustomTextField.swift
//  Pocket
//
//  Created by Haresh Vavadiya on 8/5/17.
//  Copyright © 2017 dignizant. All rights reserved.
//

import UIKit

@IBDesignable class CustomTextField: UITextField {

    @IBInspectable
    var leftPaddingView: Int {
        get{
            return self.leftPaddingView
        }
        set {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: 10))
            leftViewMode = .always
            leftView = paddingView
        }
    }
    
    @IBInspectable
    var rightPaddingView: Int {
        get{
            return self.rightPaddingView
        }
        set {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: 10))
            rightViewMode = .always
            rightView = paddingView
        }
    }

    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
