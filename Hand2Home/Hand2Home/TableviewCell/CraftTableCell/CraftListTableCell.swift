//
//  CraftListTableCell.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class CraftListTableCell: UITableViewCell {

    //MARK: - Outlet
    
    
    @IBOutlet weak var imgCraft: UIImageView!
    
    @IBOutlet weak var lblCraftName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
