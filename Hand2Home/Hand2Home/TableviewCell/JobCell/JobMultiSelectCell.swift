//
//  JobMultiSelectCell.swift
//  Hand2Home
//
//  Created by YASH on 07/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class JobMultiSelectCell: UITableViewCell {

    @IBOutlet weak var lblJob: UILabel!
    
    @IBOutlet weak var imgSelection: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
