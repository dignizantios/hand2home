//
//  NotificationCell.swift
//  Hand2Home
//
//  Created by YASH on 27/01/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblNotification: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblNotification.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
