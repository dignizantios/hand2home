//
//  SearchListCellTableViewCell.swift
//  Hand2Home
//
//  Created by YASH on 05/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class SearchListCellTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCraft: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgCraft.layer.cornerRadius = imgCraft.layer.bounds.width / 2
        imgCraft.layer.masksToBounds = true
        
        lblName.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblName.font = MySingleton.sharedManager.getFontForTypeNormalWithSize(fontSize: 18)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
