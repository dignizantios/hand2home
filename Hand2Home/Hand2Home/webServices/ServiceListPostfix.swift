//
//  ServiceListPostfix.swift
//  Hand2Home
//
//  Created by YASH on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation


//TODO: - Version 1
let categoryList = "categories_list"

let craftList = "craftsman_list"

let jobsList = "jobs_list"

let searchCraftsman = "search_craftsman"

let request_inquiry = "request_inquiry"

let notoficationlist = "notification_list"

let craftsman_details = "craftsman_details"


//TODO: - Version 2


let location = "user_location"

let categoryListV2 = "categories_v2_list"

let craftListV2 = "craftsman_v2_list"

let searchCraftmanV2 = "search_craftsman_v2"

let requestInquiryV2 = "request_inquiry_v2"

let notificationListV2 = "notification_list_v2"

let craftManDetailsV2 = "craftsman_details_v2"

let manageCraftmanStatics = "manage_craftman_statistics"


